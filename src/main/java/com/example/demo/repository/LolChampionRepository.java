package com.example.demo.repository;


import com.example.demo.entities.LolChampion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface LolChampionRepository extends JpaRepository<LolChampion,Long> {



}
