package com.example.demo.controller;

import com.example.demo.entities.LolChampion;
import com.example.demo.service.LolChampionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/lolchampion")
public class LolChampionController {
    @Autowired
    private LolChampionService lolChampionService;
    @GetMapping
    public List<LolChampion> findAll(){
        return this.lolChampionService.findAll();
    }
    @PostMapping
    public LolChampion save(@RequestBody LolChampion lolChampion){
        return this.lolChampionService.save(lolChampion);
    }


}
