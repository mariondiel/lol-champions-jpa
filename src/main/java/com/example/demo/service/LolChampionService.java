package com.example.demo.service;

import com.example.demo.entities.LolChampion;
import com.example.demo.repository.LolChampionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LolChampionService {
    @Autowired
    LolChampionRepository lolChampionRepository;

    public List<LolChampion> findAll(){
        return this.lolChampionRepository.findAll();
    }

    public LolChampion save(LolChampion lolChampion){
        System.out.println("Request to create lolChampion [" + lolChampion + "]");
        return this.lolChampionRepository.save(lolChampion);
    }

}
